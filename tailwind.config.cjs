const colors = require('tailwindcss/colors');

module.exports = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		colors: {
			...colors,
			rarity: '#DB2777',
			orange: '#FFB801',
			purple: '#C520FF',
			'green-hover': '#69F7C7',
			'green-gradient': 'linear-gradient(90deg, #33FF99 0%, #00CCFF 100%)',
			'open-sea': '#2081E2',
			'rarity-sniper': '#FC7139',
			'light-blue': '#01D1FF',

			// Dark Theme
			gray: '#3E3E3E',
			green: '#28F4AF',
			'dark-gray': '#0C0C0C',
			'medium-gray': '#1C1C1C',
			'text-teriary': '#636363',
			'text-secondary': '#AAAAAA',
			'text-primary': '#FFFFFF',
		},
		fontFamily: {
			sans: [
				'"Manrope"',
				'system-ui',
				'-apple-system',
				'BlinkMacSystemFont',
				'"Segoe UI"',
				'Roboto',
				'"Helvetica Neue"',
				'Arial',
				'"Noto Sans"',
				'sans-serif',
				'"Apple Color Emoji"',
				'"Segoe UI Emoji"',
				'"Segoe UI Symbol"',
				'"Noto Color Emoji"',
			],
		},
		fontSize: {
			// Desktop Headings
			'd-h1': ['80px', '100px'],
			'd-h2': ['60px', '70px'],
			'd-h3': ['40px', '50px'],
			'd-h4': ['20px', '30px'],
			'd-lead': ['20px', '34px'],

			// Mobile Headings
			'm-h1': ['48px', '46px'],
			'm-h2': ['32px', '40px'],
			'm-h3': ['26px', '32px'],
			'm-h4': ['20px', '26px'],

			// Titles
			'title-big': ['14px', '26px'],
			'title-medium': ['12px', '24px'],
			'title-small': ['10px', '20px'],

			// Shared but in design desktop prefix
			normal: ['18px', '30px'],
			small: ['16px', '26px'],
			xsmall: ['14px', '22px'],
			xxsmall: ['12px', '18px'],
		},
		container: {
			padding: {
				DEFAULT: '20px',
				sm: '2rem',
				lg: '4rem',
				xl: '5rem',
				'2xl': '6rem',
			},
		},
	},
	plugins: [],
};
